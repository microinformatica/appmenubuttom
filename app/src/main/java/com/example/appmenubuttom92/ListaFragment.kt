package com.example.appmenubuttom92

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ListaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListaFragment : Fragment() {
    private lateinit var listView: ListView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var adapter: ArrayAdapter<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

       setHasOptionsMenu(true)
        Log.d("MENU", "onCreate: MENU ")
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =inflater.inflate(R.layout.fragment_lista, container, false)
        listView  = view.findViewById(R.id.lstAlumnos)

       //  Lee los alumnos del Array-String  los pone en la variable items
        val items = resources.getStringArray(R.array.alumnos)

        // Incia el objeto arrayList y agrega TODOS los items
        arrayList =java.util.ArrayList()
        arrayList.addAll(items)

        // Inicia el Adaptador con estilo , y el arraylist
        adapter = ArrayAdapter(requireContext(),android.R.layout.simple_list_item_1,arrayList)
        // Asigna el adater al listView
        listView.adapter= adapter

        listView.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->

            var alumno:String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de Alumnos")
            builder.setMessage(position.toString() + ":"  + alumno)
            builder.setPositiveButton("OK") { dialog, which ->
                // Acción cuando el usuario hace clic en el botón OK
            }
            builder.show()





        })

        return view
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.searchview, menu)

        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView

        searchView.setOnQueryTextListener( object : SearchView.OnQueryTextListener{

            override fun onQueryTextSubmit(query: String?): Boolean {
                TODO("Not yet implemented")
             return  false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return true
            }
        })
        Log.d("CREADO", "onCreateOptionsMenu: ")
           super.onCreateOptionsMenu(menu, inflater)


    }




}