package com.example.appmenubuttom92

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.appmenubuttom92.DataBase.Alumno
import com.example.appmenubuttom92.DataBase.dbAlumnos



class DbFragment : Fragment() {
    private lateinit var btnGuardar : Button
    private lateinit var btnBuscar : Button
    private lateinit var txtMatricula : EditText
    private lateinit var txtNombre : EditText
    private lateinit var txtDomicilio : EditText
    private lateinit var txtEspecialidad : EditText
    private lateinit var txtUrlImagen: TextView
    private lateinit var db : dbAlumnos




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val View = inflater.inflate(R.layout.fragment_db, container, false)

        // Inflate the layout for this fragment
        btnGuardar = View.findViewById(R.id.btnGuardar)
        btnBuscar = View.findViewById(R.id.btnBuscar)
        txtMatricula = View.findViewById(R.id.txtMatricula)
        txtNombre = View.findViewById(R.id.txtNombre)
        txtDomicilio = View.findViewById(R.id.txtDomicilio)
        txtEspecialidad = View.findViewById(R.id.txtEspecialidad)
        txtUrlImagen = View.findViewById(R.id.txtFoto)
        btnGuardar.setOnClickListener {
            // validar
            if (txtNombre.text.toString().contentEquals("") ||
                txtDomicilio.text.toString().contentEquals("") ||
                txtMatricula.text.toString().contentEquals("") ||
                txtEspecialidad.text.toString().contentEquals("")
            ) {
                Toast.makeText(
                    requireContext(),
                    "Falto informacion por capturar",
                    Toast.LENGTH_SHORT
                ).show()

            } else {

                var alumnos: Alumno
                alumnos = Alumno();
                alumnos.nombre = txtNombre.text.toString()
                alumnos.matricula = txtMatricula.text.toString()
                alumnos.domicilio = txtDomicilio.text.toString()
                alumnos.foto = "Pendiente"

                db = dbAlumnos(requireContext())
                db.openDataBase()
                var id: Long = db.insertarAlumno(alumnos)
                Toast.makeText(
                    requireContext(),
                    "Se agrego con exito con ID " + id,
                    Toast.LENGTH_SHORT
                ).show()
                db.close()


            }
        }
            btnBuscar.setOnClickListener {

                // validar
               if(txtMatricula.text.toString().contentEquals("")){
                   Toast.makeText(requireContext(),"falto capturar matricula",Toast.LENGTH_SHORT).show()
               }
                else {
                   db = dbAlumnos(requireContext())
                   db.openDataBase()
                   var alumno:Alumno = Alumno()
                   alumno = db.getAlumno(txtMatricula.text.toString())
                   if(alumno.id!=0){
                       txtNombre.setText(alumno.nombre)
                       txtDomicilio.setText(alumno.domicilio)
                       txtEspecialidad.setText(alumno.especialidad)
                   }
                   else Toast.makeText(requireContext(),"No se encontro la Matricula",Toast.LENGTH_SHORT).show()



                }


            }





        return View
    }

    public fun limpiar(){
        txtNombre.text.clear()
        txtMatricula.text.clear()
        txtEspecialidad.text.clear()
        txtDomicilio.text.clear()
        txtMatricula.requestFocus()


    }




}